package invictus.control;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Button;

/**
 * A {@link Button} that gets its state from a {@link GenericHID}.
 */
public class JoystickAxisAsButton extends Button {
  private final GenericHID m_joystick;
  private final int m_axisNumber;
  private final double m_buttonThreshold;

  /**
   * Create a joystick button for triggering commands.
   *
   * @param joystick        The GenericHID object that has the button (e.g.
   *                        Joystick, KinectStick, etc)
   * @param buttonNumber    The button number (see
   *                        {@link GenericHID#getRawButton(int) }
   * @param buttonThreshold The value over which the "button" is triggered
   */
  public JoystickAxisAsButton(GenericHID joystick, int axisNumber, double buttonThreshold) {
    m_joystick = joystick;
    m_axisNumber = axisNumber;
    m_buttonThreshold = buttonThreshold;
  }

  /**
   * Gets the value of the joystick button.
   *
   * @return The value of the joystick button
   */
  @Override
  public boolean get() {
    return m_joystick.getRawAxis(m_axisNumber) > m_buttonThreshold;
  }
}