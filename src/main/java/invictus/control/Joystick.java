package invictus.control;

public class Joystick {
    public double X;
    public double Y;

    public Joystick(double x, double y) {
        X = x;
        Y = y;
    }
}