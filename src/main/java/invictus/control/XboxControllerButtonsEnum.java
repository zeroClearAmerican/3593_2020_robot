package invictus.control;

public enum XboxControllerButtonsEnum {
    A(1),
    B(2),
    X(3),
    Y(4),
    LB(5),
    RB(6),
    Select(7),
    Start(8),
    LJ(9),
    RJ(10);

    private final int value;
    private XboxControllerButtonsEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}