package invictus.motorControllers;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public class PIDTalonSRX extends WPI_TalonSRX implements PIDOutput, PIDSource {
    private PIDSourceType _pidSourcetype = PIDSourceType.kDisplacement;
    
    public PIDTalonSRX(int channelNum) {
        super(channelNum);
    }

    // PIDOutput
    @Override
    public void pidWrite(double output) {
        this.set(ControlMode.PercentOutput, output);
    }

    // PIDSource
    @Override
    public void setPIDSourceType(PIDSourceType pidSource) {
        _pidSourcetype = pidSource;
    }

    @Override
    public PIDSourceType getPIDSourceType() {
        return _pidSourcetype;
    }

    @Override
    public double pidGet() {
        return _pidSourcetype == PIDSourceType.kDisplacement
            ? this.getSelectedSensorPosition()
            : this.getSelectedSensorVelocity();
    }
}