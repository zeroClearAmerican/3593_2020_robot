package invictus.dashboard;

import java.net.InetSocketAddress;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import frc.robot.Robot;
import frc.robot.commands.CommandBase;
import frc.robot.telemetry.InboundPacket;

public class DashboardWebsocketProvider extends WebSocketServer {
  public DashboardWebsocketProvider(InetSocketAddress address) {
    super(address);
    System.out.println(">>> [DASHBOARD] Creating websocket on " + address.getHostName() + ":" + address.getPort());
  }

  @Override
  public void onOpen(WebSocket socket, ClientHandshake handshake) {
    System.out.println(">>> [DASHBOARD] New Dashboard connection from " + socket.getRemoteSocketAddress());
  }

  @Override
  public void onClose(WebSocket socket, int code, String reason, boolean remote) {
    System.out.println(">>> [DASHBOARD] Dashboard connection " + socket.getRemoteSocketAddress() + " closed - " + reason);
  }

  @Override
  public void onMessage(WebSocket socket, String message) {
    var mapper = new ObjectMapper();
    try {
      var packet = mapper.readValue(message, InboundPacket.class);
      CommandBase.Dashboard.ReceiveDashboardUpdate(packet);
    } catch (Exception e) {
      Robot.Debug("[DASHBOARD] Could not parse inbound packet");
    }
  }

  @Override
  public void onError(WebSocket socket, Exception ex) {
    System.err.println(">>> [DASHBOARD] Dashboard connection error: " + ex.getMessage());
  }

  @Override
  public void onStart() {
  }
}