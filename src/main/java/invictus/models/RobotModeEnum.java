package invictus.models;

public enum RobotModeEnum {
    Disabled,
    Test,
    Teleop,
    Autonomous
}