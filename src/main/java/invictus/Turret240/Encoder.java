package invictus.turret240;

import com.ctre.phoenix.motorcontrol.SensorCollection;

import edu.wpi.first.wpilibj.MedianFilter;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public class Encoder implements PIDSource {
    private PIDSourceType _pidSourceType = PIDSourceType.kDisplacement;
    private final SensorCollection sensors;
    private volatile int lastValue = Integer.MIN_VALUE;
    private MedianFilter filterAverager = new MedianFilter(5);

    public Encoder(SensorCollection coll) {
        sensors = coll;
    }

    public double GetPosition() {
        int raw = sensors.getPulseWidthRiseToFallUs();

        if (raw == 0) {
            int lastValue = this.lastValue;
            if (lastValue == Integer.MIN_VALUE) {
                return 0;
            }
            return lastValue;
        }

        int actualValue = Math.min(4096, raw - 128);
        lastValue = actualValue;
        try {
            return filterAverager.calculate(actualValue);
        } catch (NullPointerException e) {
            return actualValue;
        }
    }

    @Override
    public void setPIDSourceType(PIDSourceType pidSource) {
        _pidSourceType = pidSource;
    }

    @Override
    public PIDSourceType getPIDSourceType() {
        return _pidSourceType;
    }

    @Override
    public double pidGet() {
        return GetPosition();
    }
}