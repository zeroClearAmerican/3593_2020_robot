package invictus.turret240;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public class Turret240Flywheel implements PIDOutput, PIDSource {
    public CANSparkMax Motor1;
    public CANSparkMax Motor2;

    public Turret240Flywheel(int wheel1Channel, int wheel2Channel, MotorType motorType) {
        // right of the flywheel looking forward
        Motor1 = new CANSparkMax(wheel1Channel, motorType);
        Motor1.setSmartCurrentLimit(30);
        Motor1.setSecondaryCurrentLimit(40);
        Motor1.setIdleMode(IdleMode.kCoast);

        // left of the flywheel looking forward
        Motor2 = new CANSparkMax(wheel2Channel, motorType);
        Motor2.setSmartCurrentLimit(30);
        Motor2.setSecondaryCurrentLimit(40);
        Motor2.setIdleMode(IdleMode.kCoast);
        Motor2.setInverted(true);
    }

    public void set(double speed) {
        Motor1.set(speed);
        Motor2.set(speed);
    }

    public double GetRPMSpeed() {
        return Motor1.getEncoder().getVelocity();
    }

    public void StopMotors() {
        set(0);
    }

    @Override
    public void pidWrite(double output) {
        set(output);
    }

    @Override
    public void setPIDSourceType(PIDSourceType pidSource) {
        return;
    }

    @Override
    public PIDSourceType getPIDSourceType() {
        return PIDSourceType.kRate;
    }

    @Override
    public double pidGet() {
        return GetRPMSpeed();
    }
}