package frc.robot.telemetry;

import invictus.models.PidConstants;

public class Flywheel {
    public double TargetSpeed = 0;
    public double ActualSpeed = 0;
    public double HoodAngle = 0;
    public PidConstants PIDValues = new PidConstants(0, 0, 0);
}