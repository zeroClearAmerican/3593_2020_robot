package frc.robot.telemetry;

import invictus.models.PidConstants;

public class Turret {
    public double Heading = 0;
    public double Angle = 0;
    public PidConstants PIDValues = new PidConstants(0, 0, 0);
}