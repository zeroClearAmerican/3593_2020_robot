package frc.robot.telemetry;

import java.util.Dictionary;
import java.util.Hashtable;

public class Drive {
    public double Heading = 0;
    public boolean VirtualShifter = false;
    public Dictionary<String, Object> Wheels = new Hashtable<String, Object>();
}