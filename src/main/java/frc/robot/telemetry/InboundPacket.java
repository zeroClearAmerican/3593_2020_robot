package frc.robot.telemetry;

public class InboundPacket {
    public boolean turretPIDEnabled = false;    
    public double turretSetpoint = 0;
    public double turretP = 0;
    public double turretI = 0;
    public double turretD = 0;

    public double hoodAngle = 0;
    public boolean flywheelPIDEnabled = false;
    public double flywheelSetpoint = 0;
    public double flywheelP = 0;
    public double flywheelI = 0;
    public double flywheelD = 0;
}