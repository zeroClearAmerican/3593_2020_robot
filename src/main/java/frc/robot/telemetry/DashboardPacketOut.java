package frc.robot.telemetry;

public class DashboardPacketOut {
    public Power Power = new Power();
    public Match Match = new Match();
    public String RobotMode = "";
    public Drive Drive = new Drive();
    public Pneumatics Pneumatics = new Pneumatics();
    public Turret Turret = new Turret();
    public Intake Intake = new Intake();
    public Flywheel Flywheel = new Flywheel();
    public Targeting Targeting = new Targeting();
}