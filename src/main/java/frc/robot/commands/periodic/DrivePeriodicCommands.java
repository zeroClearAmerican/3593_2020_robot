package frc.robot.commands.periodic;

import frc.robot.RobotMap;
import frc.robot.commands.CommandBase;
import frc.robot.commands.auto.AutoControlValues;
import invictus.control.Utilities;
import invictus.control.XboxInput;
import invictus.models.RobotModeEnum;

public class DrivePeriodicCommands extends CommandBase {
  public DrivePeriodicCommands() {
    requires(Drive);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if (RobotMode == RobotModeEnum.Autonomous) {
      Drive.DriveSwerve(Telemetry.GetGyroHeading(), 0, AutoControlValues.DriveSTR, AutoControlValues.DriveFWD);
    } else {
      var driver = Controls.Driver;
      DriveSwerve(driver);
    }
  }

  private void DriveSwerve(XboxInput driver) {
    var gearSpeed = 1d;
    if (!Drive.VirtualHighGear) {
      gearSpeed = RobotMap.DownShiftRatio;
    }

    var RCW = driver.GetTriggerAxis() * 0.7;
    var STR = Utilities.Deadband(driver.getRawAxis(0), 0.09) * -1;
    var FWD = Utilities.Deadband(driver.getRawAxis(1), 0.09) * -1;

    Drive.DriveSwerve(
      Telemetry.GetGyroHeading(), 
      RCW * gearSpeed, 
      STR * gearSpeed, 
      FWD * gearSpeed
    );
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
