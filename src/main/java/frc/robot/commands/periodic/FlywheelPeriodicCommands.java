package frc.robot.commands.periodic;

import frc.robot.RobotMap;
import frc.robot.commands.CommandBase;

public class FlywheelPeriodicCommands extends CommandBase {
  public FlywheelPeriodicCommands() {
    requires(Flywheel);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    var operator = Controls.Operator;

    // Adjust hood
    var hoodControl = operator.getRawAxis(1);
    if (Turret.LockedOn) {
      var hoodAngleAdjustment = hoodControl * RobotMap.Controls_HoodAdjustmentRatio;
      Flywheel.SetHoodAngleByLimelightWithAdjustment(hoodAngleAdjustment - 3) ;
    } else {
      Flywheel.SetHoodPitch(hoodControl);
    }

    if (GetLimelightTargetArea() > 8 && Turret.LockedOn) {
      Turret.LockedOn = false;
      Turret.SetPIDEnabled(false);
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
