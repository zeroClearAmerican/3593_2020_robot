package frc.robot.commands.periodic;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import frc.robot.commands.CommandBase;
import frc.robot.commands.auto.AutoControlValues;
import invictus.models.RobotModeEnum;

public class IntakePeriodicCommands extends CommandBase {
  public IntakePeriodicCommands() {
    requires(Intake);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if (RobotMode == RobotModeEnum.Autonomous) {
      Intake.RunRollers(AutoControlValues.RollerSpeed);
      Intake.RunConveyor(AutoControlValues.ConveyorSpeed);
    } else {
      var operator = Controls.Operator;

      if (operator.getYButton()) {
        Intake.RunIntake(-1);
      } else {
        if (operator.getTriggerAxis(Hand.kLeft) > 0.3) {
          Intake.RunRollers(1);
        } else {
          Intake.RunRollers(0);
        }
    
        if (operator.getTriggerAxis(Hand.kRight) > 0.3) {
          Intake.RunConveyor(1);
        } else {
          Intake.RunConveyor(0);
        }
      }
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
