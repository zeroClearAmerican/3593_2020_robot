package frc.robot.commands.periodic;

import frc.robot.RobotMap;
import frc.robot.commands.CommandBase;

public class TurretPeriodicCommands extends CommandBase {
  public TurretPeriodicCommands() {
    requires(Turret);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    var operator = Controls.Operator;

    var turretControl = operator.getRawAxis(4);
    if (Turret.LockedOn || Turret.RotationPID.isEnabled()) {
      // If locked on, use limelight
      if (Turret.LockedOn) {
        if (!GetLimelightTargetSeen()) {
          Turret.SetLockOn(false);
        } else {
          var manualAdjustment = turretControl * RobotMap.Controls_TurretAdjustmentRatio;
          Turret.SetRotationWithLimelightOffset(GetLimelightTargetDelta() - 0.5, manualAdjustment);
        }
      }
    } else {
      Turret.RotateManually(turretControl * 0.7);
    }

    if (Turret.LockedOn) {
      LEDs.SetTurretStripSolidColor(0, 255, 0); 
    } else {
      LEDs.SetTurretStripSolidColor(255, 0, 0); 
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
