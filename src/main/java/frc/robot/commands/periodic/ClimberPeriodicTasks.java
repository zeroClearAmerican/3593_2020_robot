package frc.robot.commands.periodic;

import frc.robot.commands.CommandBase;

public class ClimberPeriodicTasks extends CommandBase {
  public ClimberPeriodicTasks() {
    requires(Climber);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    var operator = Controls.Operator;

    if (operator.getPOV() == 0) {
      Climber.RunMotor(1);
    } else if (operator.getPOV() == 180) {
      Climber.RunMotor(-1);
    } else if(operator.getPOV() == -1) {
      Climber.RunMotor(0);
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
