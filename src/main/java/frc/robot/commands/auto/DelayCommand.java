package frc.robot.commands.auto;

import java.time.Instant;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class DelayCommand extends Command {
  private long timeout;
  private long startTime;
  private long endTime;

  /**
   * Accepts timeout in ms as arg
   * @param timeout
   */
  public DelayCommand(long timeout) {
    this.timeout = timeout;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    startTime = Instant.now().toEpochMilli();
    endTime = startTime + timeout;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    var finished = Instant.now().toEpochMilli() >= endTime;
    if (finished) {
      Robot.Debug("[AUTO] Finished delay for " + (endTime - startTime) + "ms");
    }
    return finished;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
