package frc.robot.commands.auto;

public class AutoControlValues {
    // Intake
    public static double ConveyorSpeed = 0;
    public static double RollerSpeed = 0;

    // Turret

    // Flywheel

    // Drive
    public static double DriveSpeed = 0;
    public static double DriveDirection = 0;
    public static double DriveFWD = 0;
    public static double DriveSTR = 0;

    public static void Reset() {
        DriveSpeed = 0;
        DriveDirection = 0;
        DriveFWD = 0;
        DriveSTR = 0;
        RollerSpeed = 0;
        ConveyorSpeed = 0;
    }
}