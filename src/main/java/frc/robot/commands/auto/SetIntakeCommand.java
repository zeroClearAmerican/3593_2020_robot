package frc.robot.commands.auto;

import edu.wpi.first.wpilibj.command.Command;

public class SetIntakeCommand extends Command {
  private double _rollerSpeed = 0;
  private double _conveyorSpeed = 0;

  public SetIntakeCommand(double rollerSpeed, double conveyorSpeed) {
    _rollerSpeed = rollerSpeed;
    _conveyorSpeed = conveyorSpeed;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    AutoControlValues.RollerSpeed = _rollerSpeed;
    AutoControlValues.ConveyorSpeed = _conveyorSpeed;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return true;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
