package frc.robot.commands.auto;

import edu.wpi.first.wpilibj.command.Command;

public class SetDriveCommand extends Command {
  double STR = 0;
  double FWD = 0;

  public SetDriveCommand(double fwd, double str) {
    STR = str;
    FWD = fwd;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    AutoControlValues.DriveFWD = FWD;
    AutoControlValues.DriveSTR = STR;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return true;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
