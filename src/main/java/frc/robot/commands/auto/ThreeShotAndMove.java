
package frc.robot.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class ThreeShotAndMove extends CommandGroup {
  // addSequential(new Command2());
  // addSequential(new Command2());
  public ThreeShotAndMove() {
    // Delay for 4 seconds to let the flywheel spin up
    addSequential(new DelayCommand(4000));

    // Shoot balls
    addSequential(new SetIntakeCommand(0, 0.8));
    addSequential(new DelayCommand(3000));
    addSequential(new SetIntakeCommand(0, 0));

    // Drive backward for 1 second
    addSequential(new SetDriveCommand(-0.5, 0));
    addSequential(new DelayCommand(1000));
    addSequential(new SetDriveCommand(0, 0));
  }
}
