package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.OI;
import frc.robot.subsystems.*;
import invictus.models.RobotModeEnum;

public class CommandBase extends Command {
  public static boolean DebugModeEnabled = true;
  public static RobotModeEnum RobotMode = RobotModeEnum.Disabled;

  public static OI Controls;
  
  public static TelemetrySubsystem Telemetry;
  public static DriveSubsystem Drive;
  public static PneumaticsSubsystem Pneumatics;
  public static IntakeSubsystem Intake;
  public static TurretSubsystem Turret;
  public static FlywheelSubsystem Flywheel;
  public static ClimberSubsystem Climber;
  public static LEDSubsystem LEDs;

  // Must be initialized last
  public static DashboardSubsystem Dashboard;

  public static NetworkTable LimelightTable;

  // Instantiates static instances of all subsystems
  public static void InitSubsystems() {
    Controls = new OI();
    Drive = new DriveSubsystem();
    Telemetry = new TelemetrySubsystem();
    Pneumatics = new PneumaticsSubsystem();
    Intake = new IntakeSubsystem();
    Turret = new TurretSubsystem();
    Flywheel = new FlywheelSubsystem();
    Climber = new ClimberSubsystem();
    LEDs = new LEDSubsystem();

    // Must be initialized last
    Dashboard = new DashboardSubsystem();

    LimelightTable = NetworkTableInstance.getDefault().getTable("limelight");
  }

  // DOESN'T RUN
  @Override
  protected void execute() { }

  public static double GetLimelightTargetDelta() {
    return LimelightTable.getEntry("tx").getNumber(Integer.MIN_VALUE).doubleValue();
  }

  public static double GetLimelightTargetArea() {
    return LimelightTable.getEntry("ta").getNumber(Integer.MIN_VALUE).doubleValue();
  }

  public static boolean GetLimelightTargetSeen() {
    return LimelightTable.getEntry("tv").getNumber(Integer.MIN_VALUE).intValue() == 1;
  }

  @Override
  protected boolean isFinished() {
    return false;
  }
}
