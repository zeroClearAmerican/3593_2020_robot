package frc.robot.commands.singular;

import frc.robot.Robot;
import frc.robot.commands.CommandBase;

public class ToggleVirtualGearShiftCommand extends CommandBase {
  public ToggleVirtualGearShiftCommand() {
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Drive.VirtualHighGear = !Drive.VirtualHighGear;
    Robot.Debug("[COMMAND] Virtual gear toggled");
  }

  @Override
  protected void execute() {
  }

  @Override
  protected boolean isFinished() {
    return true;
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
