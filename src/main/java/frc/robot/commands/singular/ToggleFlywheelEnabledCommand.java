package frc.robot.commands.singular;

import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.commands.CommandBase;

public class ToggleFlywheelEnabledCommand extends CommandBase {
  public ToggleFlywheelEnabledCommand() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    if (Flywheel.WheelPID.isEnabled()) {
      if (!DriverStation.getInstance().isFMSAttached()) {
        Flywheel.SetPIDEnabled(false);
      }
    } else {
      Flywheel.SetPIDEnabled(true);
      Flywheel.SetRPMTarget(2500);
    }
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return true;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
