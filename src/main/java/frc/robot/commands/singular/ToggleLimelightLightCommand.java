package frc.robot.commands.singular;

import frc.robot.Robot;
import frc.robot.commands.CommandBase;

public class ToggleLimelightLightCommand extends CommandBase {
  public ToggleLimelightLightCommand() {
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Flywheel.ToggleLimelightLights();
    Robot.Debug("[COMMAND] Limelight lights toggled");
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return true;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
