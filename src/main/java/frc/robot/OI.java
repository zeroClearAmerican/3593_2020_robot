package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.singular.*;
import invictus.control.JoystickAxisAsButton;
import invictus.control.XboxControllerButtonsEnum;
import invictus.control.XboxInput;

public class OI {
  public XboxInput Driver;
  public XboxInput Operator;

  public OI() {
    Driver = new XboxInput(0);
    Operator = new XboxInput(1);

    // === Driver
    GetJoystickButton(Driver, XboxControllerButtonsEnum.RB.getValue()).whenPressed(new ResetGyroCommand());
    GetJoystickButton(Driver, XboxControllerButtonsEnum.LB.getValue()).whenPressed(new ToggleVirtualGearShiftCommand());

    // === Operator
    GetJoystickButton(Operator, XboxControllerButtonsEnum.X.getValue()).whenPressed(new ToggleRollerPositionCommand());
    GetJoystickButton(Operator, XboxControllerButtonsEnum.A.getValue()).whenPressed(new ToggleTurretLockedOnCommand());
    GetJoystickButton(Operator, XboxControllerButtonsEnum.Select.getValue()).whenPressed(new ToggleLimelightLightCommand());
    GetJoystickButton(Operator, XboxControllerButtonsEnum.B.getValue()).whenPressed(new ToggleFlywheelEnabledCommand());
    GetJoystickButton(Operator, XboxControllerButtonsEnum.Start.getValue()).whenPressed(new ToggleClimberBrakeCommand());
  }

  public JoystickButton GetJoystickButton(XboxController controller, int buttonIndex) {
    return new JoystickButton(controller, buttonIndex);
  }

  public JoystickAxisAsButton GetJoystickAxisAsButton(XboxController controller, int axisNumber, double valueThreshold) {
    return new JoystickAxisAsButton(controller, axisNumber, valueThreshold);
  }
}
