package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Scheduler;
import frc.robot.commands.CommandBase;
import frc.robot.commands.auto.AutoControlValues;
import frc.robot.commands.auto.ThreeShotAndMove;
import invictus.models.RobotModeEnum;

public class Robot extends TimedRobot {
  public static String SelectedAutoMode = "threeShot";
  private CommandGroup autoMode;
  public AutoControlValues AutoControls;

  @Override
  public void robotInit() {
    CommandBase.InitSubsystems();
    CommandBase.RobotMode = RobotModeEnum.Disabled;

    CommandBase.Telemetry.Gyro.calibrate();
    CommandBase.Telemetry.Gyro.reset();
  }

  @Override
  public void robotPeriodic() {
  }

  @Override
  public void disabledInit() {
    CommandBase.RobotMode = RobotModeEnum.Disabled;
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void autonomousInit() {
    switch (SelectedAutoMode) {
      case "threeShot": {
        autoMode = new ThreeShotAndMove();
        break;
      }
    }
    AutoControlValues.Reset();
    CommandBase.Telemetry.Gyro.reset();

    CommandBase.RobotMode = RobotModeEnum.Autonomous;
    CommandBase.Flywheel.SetRPMTarget(2600);

    CommandBase.Turret.SetLockOn(true);
    Timer.delay(1.8);
    autoMode.start();
  }

  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
    CommandBase.RobotMode = RobotModeEnum.Teleop;

    if (DriverStation.getInstance().isFMSAttached()) {
      CommandBase.Flywheel.SetRPMTarget(2500);
    }

    CommandBase.Turret.SetLockOn(false);

    CommandBase.Climber.SetBrake(true);
  }

  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void testInit() {
    CommandBase.RobotMode = RobotModeEnum.Test;
  }

  @Override
  public void testPeriodic() {
  }

  public static void Debug(String message, StackTraceElement[] stackTrace) {
    System.out.println(">>> " + message);

    if (CommandBase.DebugModeEnabled) {
      DriverStation.reportError(">>> " + message, stackTrace);
    }
  }

  public static void Debug(String message) {
    System.out.println(">>> " + message);
  }
}
