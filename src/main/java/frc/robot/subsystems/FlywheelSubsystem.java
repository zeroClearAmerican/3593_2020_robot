package frc.robot.subsystems;

import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.CommandBase;
import frc.robot.commands.periodic.FlywheelPeriodicCommands;
import invictus.turret240.Turret240Flywheel;
import invictus.models.PidConstants;

public class FlywheelSubsystem extends Subsystem {
  public Turret240Flywheel Flywheel;
  public PIDController WheelPID;
  public Servo TurretHood1;
  public Servo TurretHood2;

  public boolean LimelightLightsOn = true;

  public FlywheelSubsystem() {
    // Flywheel
    try {
      Flywheel = new Turret240Flywheel(RobotMap.Turret_Flywheel1, RobotMap.Turret_Flywheel2, MotorType.kBrushless);
    } catch (Exception ex) {
      Robot.Debug("[TURRET] Error initializing turret flywheel", ex.getStackTrace());
      throw ex;
    }

    // Flywheel PID
    try {
      PidConstants flywheelPid = RobotMap.PID_Turret_Flywheel;
      WheelPID = new PIDController(flywheelPid.kP, flywheelPid.kI, flywheelPid.kD, Flywheel, Flywheel);
      WheelPID.setF(flywheelPid.kF);
      WheelPID.setSubsystem("Flywheel PID");
      WheelPID.setPIDSourceType(PIDSourceType.kRate);
      WheelPID.setInputRange(-5000, 5650);
      WheelPID.setOutputRange(0, 1);
      WheelPID.setPercentTolerance(1);
      WheelPID.setSetpoint(2500);
      SmartDashboard.putData(WheelPID);
    } catch (Exception ex) {
      DriverStation.reportError("[TURRET] Error configuring flywheel PID controller: " + ex.getMessage(), true);
      Robot.Debug("[TURRET] Error configuring flywheel PID controller: " + ex.getMessage());
      throw ex;
    }

    TurretHood1 = new Servo(RobotMap.Turret_HoodServo1);
    TurretHood2 = new Servo(RobotMap.Turret_HoodServo2);

    TurretHood1.setSafetyEnabled(false);
    TurretHood2.setSafetyEnabled(false);

    TurretHood1.setBounds(2.0, 1.8, 1.5, 1.2, 1.0);
    TurretHood2.setBounds(2.0, 1.8, 1.5, 1.2, 1.0);

    SetHoodAngle(50);
  }

  public double GetRPMs() {
    return Flywheel.GetRPMSpeed();
  }

  public double GetHoodAngle() {
    var cPos = -(TurretHood1.get() + 1d);
    var scalar = 2d / 23d;
    var angle = (cPos / scalar) + 27d;
    return angle;
  }

  public void SetSpeed(double speed) {
    if (!WheelPID.isEnabled()) {
      Flywheel.set(speed);
    } 
  }

  public void SetRPMTarget(double rpms) {
    if (!WheelPID.isEnabled()) {
      WheelPID.enable();
    }

    WheelPID.setSetpoint(rpms);
  }

  public void SetPIDEnabled(boolean enabled) {
    WheelPID.setEnabled(enabled);
  }

  public void SetPIDValues(PidConstants pid) {
    WheelPID.setPID(pid.kP, pid.kI, pid.kD);
  }

  public void SetHoodPitch(double pitch) {
    TurretHood1.setSpeed(pitch);
    TurretHood2.setSpeed(pitch);
  }

  public void SetHoodAngle(double angle) {
    if (angle > 50) {
      TurretHood1.setSpeed(-1);
      TurretHood2.setSpeed(-1);
      return;
    }

    if (angle < 27) {
      TurretHood1.setSpeed(1);
      TurretHood2.setSpeed(1);
      return;
    }

    double scalar = 2d / 23d;
    double scaled2 = (angle - 27) * scalar;
    double finalValue = -(scaled2 - 1d);

    if (finalValue > 1 || finalValue < -1) {
      Robot.Debug("Error calculating hood angle. Final Value: " + finalValue);
    } else {
      TurretHood1.setSpeed(finalValue);
      TurretHood2.setSpeed(finalValue);
    }
  }

  public void SetHoodAngleByLimelight() {
    SetHoodAngleByLimelightWithAdjustment(0);
  }

  public void SetHoodAngleByLimelightWithAdjustment(double manualAdjustment) {
    var area = CommandBase.GetLimelightTargetArea();
    double slope = (RobotMap.TurretMaxHoodAngle - RobotMap.TurretMinHoodAngle) / (4.15 - 0.78);
    var out = RobotMap.TurretMinHoodAngle + slope * (area - 0.78);
    var adjustedOut = out + manualAdjustment;
    SetHoodAngle(adjustedOut);
  }

  public void ToggleLimelightLights() {
    LimelightLightsOn = !LimelightLightsOn;
    CommandBase.LimelightTable.getEntry("ledMode").forceSetValue(LimelightLightsOn ? 3 : 1);
  }

  public void StopMotors() {
    if (WheelPID.isEnabled()) {
      WheelPID.disable();
    }

    Flywheel.StopMotors();
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new FlywheelPeriodicCommands());
  }
}
