package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.periodic.ClimberPeriodicTasks;

public class ClimberSubsystem extends Subsystem {
  public WPI_TalonSRX Motor;
  public Solenoid Brake;

  public ClimberSubsystem() {
    Brake = new Solenoid(0);
    Motor = new WPI_TalonSRX(RobotMap.ClimberTalonChannel);
    Motor.setInverted(true);
  }

  public void RunMotor(double speed) {
    if (!Brake.get()) {
      speed = 0;
    }
    Motor.set(ControlMode.PercentOutput, speed);
  }

  public void SetBrake(boolean engaged) {
    Brake.set(engaged);
  }

  public void ToggleBrake() {
    if (Brake.get()) {
      Brake.set(false);
    } else {
      Brake.set(true);
    }
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ClimberPeriodicTasks());
  }
}
