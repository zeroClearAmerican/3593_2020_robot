package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.MedianFilter;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.periodic.TurretPeriodicCommands;
import invictus.control.Utilities;
import invictus.models.PidConstants;
import invictus.motorControllers.PIDTalonSRX;
import invictus.turret240.Encoder;

/**
 * Does turret stuff.
 */
public class TurretSubsystem extends Subsystem implements PIDOutput, PIDSource {
  public PIDTalonSRX Rotator;
  public Encoder Encoder;
  public PIDController RotationPID;
  public boolean LockedOn = false;

  public TurretSubsystem() {
    // Turret rotator motor
    try {
      Rotator = new PIDTalonSRX(RobotMap.Turret_Rotator);
      Rotator.configOpenloopRamp(0.1);
      Rotator.configPeakCurrentDuration(100);
      Rotator.configPeakCurrentLimit(30);
      Rotator.configContinuousCurrentLimit(20);
      Rotator.enableCurrentLimit(true);

      Encoder = new Encoder(Rotator.getSensorCollection());
      // Robot.Debug("[TURRET] Rotator motor and encoder ready");
    } catch (Exception ex) {
      DriverStation.reportError("[TURRET] Error initializing/configuring turret rotator: " + ex.getMessage(), true);
      Robot.Debug("[TURRET] Error initializing/configuring turret rotator: " + ex.getMessage());
      throw ex;
    }

    // Rotator PID
    try {
      PidConstants rotatorPid = RobotMap.PID_Turret_Rotator;
      RotationPID = new PIDController(rotatorPid.kP, rotatorPid.kI, rotatorPid.kD, this, this);
      RotationPID.setF(rotatorPid.kF);
      RotationPID.setSubsystem("Turret Rotation PID");
      RotationPID.setInputRange(0, 4096);
      RotationPID.setOutputRange(-0.7, 0.7);
      RotationPID.setAbsoluteTolerance(0.5);
      RotationPID.setContinuous(true);
      RotationPID.setSetpoint(GetAngle());
      // SmartDashboard.putData(RotationPID);
      // Robot.Debug("[TURRET] Rotation PID controller enabled");
    } catch (Exception ex) {
      DriverStation.reportError("[TURRET] Error configuring turret rotation PID controller: " + ex.getMessage(), true);
      Robot.Debug("[TURRET] Error configuring turret rotation PID controller: " + ex.getMessage());
      throw ex;
    }

    Robot.Debug("[TURRET] Turret subsystem started");
  }

  public double GetHeading() {
    return Encoder.GetPosition();
  }

  public double GetAngle() {
    var pos = GetHeading();
    double adjPos = pos;
    if (pos < RobotMap.T_Rotate_RightmostLimit + 100) {
      adjPos = 4096 + pos;
    }
    adjPos -= RobotMap.T_Rotate_LeftmostLimit;

    // Calculate to an angle where leftmost limit = 0°
    double angleAdjSlope = 360d / 4096d;
    double leftAngle = angleAdjSlope * adjPos;

    return leftAngle;
  }

  public void SetRotationWithLimelightOffset(double targetOffset, double manualAdjustment) {
    var targetAngle = GetAngle() + (targetOffset * 2);
    SetAngle(targetAngle + Utilities.Deadband(manualAdjustment, 0.1));
  }

  public void ToggleLockOn() {
    if (LockedOn) {
      LockedOn = false;
      SetPIDEnabled(false);
    } else {
      LockedOn = true;
      SetPIDEnabled(true);
    }
  }

  public void SetLockOn(boolean enabled) {
    LockedOn = enabled;
    SetPIDEnabled(enabled);
  }

  public void SetAngle(double angle) {
    if (angle > RobotMap.T_Angle_RightmostLimit) {
      angle = RobotMap.T_Angle_RightmostLimit;
    }

    if (angle < RobotMap.T_Angle_LeftmostLimit) {
      angle = RobotMap.T_Angle_LeftmostLimit;
    }

    if (!RotationPID.isEnabled()) {
      SetPIDEnabled(true);
    }

    RotationPID.setSetpoint(angle);
  }

  public void SetPIDEnabled(boolean enabled) {
    RotationPID.setEnabled(enabled);
  }

  public void SetPIDValues(PidConstants pid) {
    RotationPID.setPID(pid.kP, pid.kI, pid.kD);
  }

  public void RotateManually(double speed) {
    var cushion = 20 + (Math.abs(speed) * 25);
    var angle = GetAngle();

    var speedDirectionRight = speed > 0;
    var speedDirectionLeft = speed < 0;
    
    var proximityToLeftLimit = Math.abs(RobotMap.T_Angle_LeftmostLimit + angle);
    var proximityToRightLimit = Math.abs(RobotMap.T_Angle_RightmostLimit - angle);

    if (speedDirectionLeft && proximityToLeftLimit < cushion) {
      Robot.Debug("[TURRET] Left limit reached");
      speed = 0;
    } else if (speedDirectionRight && (proximityToRightLimit < (cushion + 10) || proximityToRightLimit > 340)) {
      Robot.Debug("[TURRET] Right limit reached");
      speed = 0;
    }
    
    if (proximityToLeftLimit < 45 || (proximityToLeftLimit < cushion || proximityToLeftLimit > 340)) {
      speed *= 0.5;
    }

    Rotator.set(ControlMode.PercentOutput, Utilities.Deadband(speed, 0.1));
  }

  public void StopAllMotors() {
    if (RotationPID.isEnabled()) {
      RotationPID.disable();
    }

    Rotator.set(ControlMode.Disabled, 0);
  }

  // PID methods

  @Override
  public void pidWrite(double output) {
    RotateManually(output);
  }

  @Override
  public void setPIDSourceType(PIDSourceType pidSource) {
    return;
  }

  @Override
  public PIDSourceType getPIDSourceType() {
    return PIDSourceType.kDisplacement;
  }

  @Override
  public double pidGet() {
    return GetAngle();
  }

  // Default command
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new TurretPeriodicCommands());
  }
}
