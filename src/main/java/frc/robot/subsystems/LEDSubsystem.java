package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;

public class LEDSubsystem extends Subsystem {
  public AddressableLED TurretStrip;
  public AddressableLEDBuffer TurretBuffer;

  public LEDSubsystem() {
    TurretStrip = new AddressableLED(RobotMap.TurretLEDChannel);
    TurretStrip.setLength(RobotMap.TurretLEDCount);

    TurretBuffer = new AddressableLEDBuffer(RobotMap.TurretLEDCount);
    TurretStrip.setData(TurretBuffer);
    TurretStrip.start();
  }

  public void SetTurretStripSolidColor(int red, int green, int blue) {
    for (int x = 0; x < RobotMap.TurretLEDCount; x++) {
      TurretBuffer.setRGB(x, red, green, blue);
    }
    TurretStrip.setData(TurretBuffer);
  }

  @Override
  public void initDefaultCommand() {
  }
}
