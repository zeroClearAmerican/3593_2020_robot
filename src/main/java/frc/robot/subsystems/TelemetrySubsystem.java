package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Subsystem for all robot sensors
 */
public class TelemetrySubsystem extends Subsystem {
  public AHRS Gyro;
  public PowerDistributionPanel PDP;
  public DriverStation DriverStationInstance;

  public TelemetrySubsystem() {
    Gyro = new AHRS(SerialPort.Port.kUSB);
    Gyro.calibrate();
    Gyro.reset();
    PDP = new PowerDistributionPanel(1);
    DriverStationInstance = DriverStation.getInstance();
  }

  public void ResetGyro() {
    Gyro.reset();
  }

  /**
   * Gets gyro angle from 0°-360° clockwise
   */
  public double GetGyroHeading() {
    double finalAngle = 0;
    double currentReading = Gyro.getAngle();

    if (currentReading > 360) {
        finalAngle = currentReading % 360;
    } else if (currentReading < 0) {
        finalAngle = 360 - (Math.abs(currentReading) % 360);
    } else {
        finalAngle = currentReading;
    }

    return finalAngle;
  }

  public double[] GetPDPChannelsCurrent() {
    double[] result = new double[16];

    for (int x = 0; x < 16; x++) {
      result[x] = PDP.getCurrent(x);
    }

    return result;
  }

  public String GetAllianceColor() {
    return DriverStationInstance.getAlliance().toString();
  }

  public double GetMatchTime() {
    return DriverStationInstance.getMatchTime();
  }

  @Override
  protected void initDefaultCommand() { }
}
