package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

// import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.periodic.IntakePeriodicCommands;

/**
 * Controls intake motors
 */
public class IntakeSubsystem extends Subsystem {
  public CANSparkMax Rollers;
  public VictorSPX LowerBelt;
  public CANSparkMax UpperBelt;
  // public DigitalInput IntakeBeam;
  // public DigitalInput OutputBeam;

  private int _ballCount = 0;
  // private boolean _intakeBeamBroken = false;
  // private boolean _outputBeamBroken = false;

  public IntakeSubsystem() {
    Rollers = new CANSparkMax(RobotMap.Intake_Rollers, MotorType.kBrushless);
    Rollers.setOpenLoopRampRate(0.1);
    Rollers.setSmartCurrentLimit(30);
    Rollers.setSecondaryCurrentLimit(40);
    Rollers.setInverted(true);

    LowerBelt = new VictorSPX(RobotMap.Intake_LowerBelt);
    LowerBelt.setInverted(false);

    UpperBelt = new CANSparkMax(RobotMap.Intake_UpperBelt, MotorType.kBrushless);
    UpperBelt.setOpenLoopRampRate(0.1);
    
    // IntakeBeam = new DigitalInput(RobotMap.Intake_BeamIn);
    // OutputBeam = new DigitalInput(RobotMap.Intake_BeamOut);
  }

  public void RunIntake(double speed) {
    Rollers.set(speed);
    LowerBelt.set(ControlMode.PercentOutput, speed);
    UpperBelt.set(speed);
  }

  public void RunRollers(double speed) {
    Rollers.set(speed);
  }

  public void RunConveyor(double speed) {
    LowerBelt.set(ControlMode.PercentOutput, speed);
    UpperBelt.set(speed);
  }

  public void CheckBeamSensors() {
    // boolean oldIntakeValue = _intakeBeamBroken;
    // boolean newIntakeValue = IntakeBeam.get();

    // boolean oldOutputValue = _outputBeamBroken;
    // boolean newOutputValue = OutputBeam.get();

    // if (oldIntakeValue && !newIntakeValue) {
    //   _ballCount++;
    // }

    // if (oldOutputValue && !newOutputValue) {
    //   _ballCount--;
    // }

    // _intakeBeamBroken = newIntakeValue;
    // _outputBeamBroken = newOutputValue;
  }

  public int GetBallCount() {
    return _ballCount;
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new IntakePeriodicCommands());
  }
}
