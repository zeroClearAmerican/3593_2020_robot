package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Subsystem for pneumatics.
 */
public class PneumaticsSubsystem extends Subsystem {
  private Solenoid _pickup;
  private Compressor _compressor;

  public PneumaticsSubsystem() {
    _pickup = new Solenoid(1);
    _compressor = new Compressor();
    _compressor.setClosedLoopControl(true);
  }

  public void TogglePickup() {
    boolean pickupStatus = _pickup.get();
    _pickup.set(!pickupStatus);
  }

  public void SetPickup(boolean out) {
    _pickup.set(out);
  }

  public boolean GetPressureSwitch() {
    return _compressor.getPressureSwitchValue();
  }

  public boolean GetIntakeRollersEngaged() {
    return _pickup.get();
  }

  @Override
  public void initDefaultCommand() { }
}
