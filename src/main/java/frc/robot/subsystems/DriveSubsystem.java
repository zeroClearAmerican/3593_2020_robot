package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.periodic.DrivePeriodicCommands;
import invictus.drive.swerve.SwerveChassis;

public class DriveSubsystem extends Subsystem {
  public SwerveChassis Chassis;
  public boolean VirtualHighGear = false;

  public DriveSubsystem() {
    try {
      Chassis = new SwerveChassis(
        RobotMap.FrontRightWheelMap, 
        RobotMap.FrontLeftWheelMap, 
        RobotMap.RearRightWheelMap,
        RobotMap.RearLeftWheelMap, 
        RobotMap.PID_Drive_SwerveRotatorConstants
      );
    } catch (Exception ex) {
      DriverStation.reportError("Error initializing drive system: " + ex.getMessage(), ex.getStackTrace());
    }
  }

  public void DriveSwerve(double gyroAngle, double RCW, double STR, double FWD) {
    Chassis.DriveHolonomic(gyroAngle, RCW, STR, FWD);
  }

  public void DriveStrafe(double gyroAngle, double angle, double speed) {
    var STR = speed * Math.cos(Math.toRadians(angle));
    var FWD = speed * Math.sin(Math.toRadians(angle));
    
    Chassis.DriveHolonomic(gyroAngle, 0, STR, FWD);
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new DrivePeriodicCommands());
  }
}
