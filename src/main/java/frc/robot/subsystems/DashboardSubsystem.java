package frc.robot.subsystems;

import java.net.InetSocketAddress;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.java_websocket.server.WebSocketServer;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.CommandBase;
import frc.robot.telemetry.*;
import invictus.dashboard.DashboardWebsocketProvider;
import invictus.models.PidConstants;

/**
 * Subsystem for two-way communication with the dashboard frontend 
 */
public class DashboardSubsystem extends Subsystem {
  public WebSocketServer WebsocketProvider;
  private Thread _updateThread;

  public DashboardSubsystem() {
    Thread t = new Thread(RunDashboardServer());
    t.start();
  }

  private Runnable RunDashboardServer() {
    return () -> {
      while (true) {
        WebsocketProvider = new DashboardWebsocketProvider(new InetSocketAddress("0.0.0.0", 5801));
        WebsocketProvider.setReuseAddr(true);
        System.out.println(">>> [DASHBOARD] websockets server starting");

        // Start new update thread
        _updateThread = new Thread(UpdateDashboard());
        _updateThread.start();
        WebsocketProvider.run();

        // If this is reached, the server has failed
        // Interrupt update thread
        _updateThread.interrupt();
        System.out.println(">>> [DASHBOARD] websockets server failed");

        try {
          Thread.sleep(300);
        } catch (InterruptedException e) {
        }
      }
    };
  }

  private Runnable UpdateDashboard() {
    return () -> {
      try {
        while (_updateThread.isAlive() && !_updateThread.isInterrupted()) {
          SendUpdate(BuildDashboardPacket());
          Thread.sleep(RobotMap.DashboardUpdateInterval);
        }
      } catch (Exception ex) {
        Robot.Debug("[DASHBOARD] Update thread failed", ex.getStackTrace());
      }
    };
  }

  public void SendUpdate(String json) {
    if (WebsocketProvider != null) {
      WebsocketProvider.broadcast(json);
    }
  }

  public static String BuildDashboardPacket() {
    var packet = new DashboardPacketOut();

    packet.Power.CurrentPerChannel = CommandBase.Telemetry.GetPDPChannelsCurrent();
    packet.Power.BatteryVoltage = RobotController.getBatteryVoltage();

    packet.Match.Time = CommandBase.Telemetry.GetMatchTime();
    packet.Match.AllianceColor = CommandBase.Telemetry.GetAllianceColor();

    packet.RobotMode = CommandBase.RobotMode.toString();

    packet.Drive.Heading = CommandBase.Telemetry.GetGyroHeading();
    packet.Drive.VirtualShifter = CommandBase.Drive.VirtualHighGear;
    packet.Drive.Wheels = CommandBase.Drive.Chassis.GetWheelStats();

    packet.Turret.Angle = CommandBase.Turret.GetAngle();
    packet.Turret.Heading = CommandBase.Turret.GetHeading();
    packet.Turret.PIDValues = new PidConstants(
      CommandBase.Turret.RotationPID.getP(), 
      CommandBase.Turret.RotationPID.getI(), 
      CommandBase.Turret.RotationPID.getD()
    );

    packet.Intake.BallCount = 0;
    packet.Intake.Engaged = CommandBase.Pneumatics.GetIntakeRollersEngaged();

    packet.Flywheel.PIDValues = new PidConstants(
      CommandBase.Flywheel.WheelPID.getP(), 
      CommandBase.Flywheel.WheelPID.getI(), 
      CommandBase.Flywheel.WheelPID.getD()
    );
    packet.Flywheel.TargetSpeed = CommandBase.Flywheel.WheelPID.getSetpoint();
    packet.Flywheel.ActualSpeed = CommandBase.Flywheel.GetRPMs();
    packet.Flywheel.HoodAngle = CommandBase.Flywheel.GetHoodAngle();

    packet.Targeting.TargetArea = CommandBase.GetLimelightTargetArea();
    packet.Targeting.TargetDelta = CommandBase.GetLimelightTargetDelta();
    packet.Targeting.LockedOn = CommandBase.Turret.LockedOn;
    packet.Targeting.TargetPresent = CommandBase.GetLimelightTargetSeen();

    ObjectMapper mapper = new ObjectMapper();
    try {
      String packetString = mapper.writeValueAsString(packet);
      return packetString;  
    } catch (JsonProcessingException e) {
      String debugMessage = "Couldn't serialize dashboard outbound packet";
      Robot.Debug(debugMessage);
      DriverStation.reportError(debugMessage, null);
      return "{ \"error\": \"Couldn't build telemetry packet\" }";
    }
  }

  public void ReceiveDashboardUpdate(InboundPacket packet) {
    Robot.Debug(" Update received from Dashboard");

    CommandBase.Flywheel.SetRPMTarget(packet.flywheelSetpoint);
    CommandBase.Flywheel.SetPIDEnabled(packet.flywheelPIDEnabled);
    CommandBase.Flywheel.SetHoodAngle(packet.hoodAngle);
    CommandBase.Flywheel.SetPIDValues(new PidConstants(packet.flywheelP, packet.flywheelI, packet.flywheelD));

    CommandBase.Turret.SetAngle(packet.turretSetpoint);
    CommandBase.Turret.SetPIDEnabled(packet.turretPIDEnabled);
    CommandBase.Turret.SetPIDValues(new PidConstants(packet.turretP, packet.turretI, packet.turretD));
  }

  @Override
  public void initDefaultCommand() { }
}
